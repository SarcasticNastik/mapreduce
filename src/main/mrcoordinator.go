package main

//
// start the coordinator process, which is implemented
// in ../mr/coordinator.go
//
// go run mrcoordinator.go pg*.txt
//
// Please do not change this file.
//

import "mapreduce/mr"
import "time"
import "os"
import "fmt"

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "Usage: mrcoordinator inputfiles...\n")
		os.Exit(1)
	}

	m := mr.MakeCoordinator(os.Args[1:], 10)
	for m.Done() == false {
		time.Sleep(time.Second)
		mapProgress := m.MapProgress()
		reduceProgress := m.ReduceProgress()
		if reduceProgress == 0 {
			fmt.Printf("\rMap Progress: %v%%", mapProgress*100)
		} else {
			fmt.Printf("\rMap Progress: %v%%, Reduce Progress: %v%%", mapProgress*100, reduceProgress*100)
		}
	}
	fmt.Printf("\rMap Progress: %v%%, Reduce Progress: %v%%\n", 100, 100)
	time.Sleep(time.Second)
}
