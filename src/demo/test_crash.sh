#!/usr/bin/env bash

# timeout command
TIMEOUT="timeout -k 2s 180s"
RACE=-race
failed_any=0

# run the test in a fresh sub-directory.
rm -rf mr-tmp
mkdir mr-tmp || exit 1
cd mr-tmp || exit 1
rm -f mr-*
cd ..

# make sure software is freshly built.
(cd .. && make mr) || exit 1
(cd ../applications && go build $RACE -buildmode=plugin crash.go) || exit 1
(cd ../applications && go build $RACE -buildmode=plugin nocrash.go) || exit 1

cd mr-tmp
rm -f mr-*

echo '***' Starting crash test.

# generate the correct output
../../main/mrsequential ../../applications/nocrash.so ../../text_files/*txt || exit 1
sort mr-out-0 > mr-correct-crash.txt
rm -f mr-out*

rm -f mr-done
($TIMEOUT ../../main/mrcoordinator ../../text_files/*txt ; touch mr-done ) &
sleep 1

# start multiple workers
$TIMEOUT ../../main/mrworker ../../applications/crash.so &

# mimic rpc.go's coordinatorSock()
SOCKNAME=/var/tmp/mapreduce-mr-`id -u`

( while [ -e $SOCKNAME -a ! -f mr-done ]
  do
    $TIMEOUT ../../main/mrworker ../../applications/crash.so
    sleep 1
  done ) &

( while [ -e $SOCKNAME -a ! -f mr-done ]
  do
    $TIMEOUT ../../main/mrworker ../../applications/crash.so
    sleep 1
  done ) &

while [ -e $SOCKNAME -a ! -f mr-done ]
do
  $TIMEOUT ../../main/mrworker ../../applications/crash.so
  sleep 1
done

wait

rm $SOCKNAME
sort mr-out* | grep . > mr-crash-all
if cmp mr-crash-all mr-correct-crash.txt
then
  echo '---' crash test: PASS
else
  echo '---' crash output is not the same as mr-correct-crash.txt
  echo '---' crash test: FAIL
  failed_any=1
fi

wait

exit $failed_any