#!/usr/bin/env bash

# timeout command
TIMEOUT="timeout -k 2s 180s"
RACE=-race
failed_any=0

# run the test in a fresh sub-directory.
rm -rf mr-tmp
mkdir mr-tmp || exit 1
cd mr-tmp || exit 1
rm -f mr-*
cd ..

# make sure software is freshly built.
(cd .. && make mr) || exit 1
(cd ../applications && go build $RACE -buildmode=plugin jobcount.go) || exit 1

cd mr-tmp

echo '***' Starting job count test.

rm -f mr-*

$TIMEOUT ../../main/mrcoordinator ../../text_files/*txt &
sleep 1

$TIMEOUT ../../main/mrworker ../../applications/jobcount.so &
$TIMEOUT ../../main/mrworker ../../applications/jobcount.so
$TIMEOUT ../../main/mrworker ../../applications/jobcount.so &
$TIMEOUT ../../main/mrworker ../../applications/jobcount.so

NT=`cat mr-out* | awk '{print $2}'`
if [ "$NT" -eq "8" ]
then
  echo '---' job count test: PASS
else
  echo '---' map jobs ran incorrect number of times "($NT != 8)"
  echo '---' job count test: FAIL
  failed_any=1
fi

wait

exit $failed_any