#!/usr/bin/env bash

# timeout command
TIMEOUT="timeout -k 2s 180s"
RACE=-race
failed_any=0

# run the test in a fresh sub-directory.
rm -rf mr-tmp
mkdir mr-tmp || exit 1
cd mr-tmp || exit 1
rm -f mr-*
cd ..

# make sure software is freshly built.
(cd .. && make mr) || exit 1
(cd ../applications && go build $RACE -buildmode=plugin mtiming.go) || exit 1

cd mr-tmp
rm -f mr-*

echo '***' Starting map parallelism test.

$TIMEOUT ../../main/mrcoordinator ../../text_files/*txt &
sleep 1

$TIMEOUT ../../main/mrworker ../../applications/mtiming.so &
$TIMEOUT ../../main/mrworker ../../applications/mtiming.so

NT=`cat mr-out* | grep '^times-' | wc -l | sed 's/ //g'`
if [ "$NT" != "2" ]
then
  echo '---' saw "$NT" workers rather than 2
  echo '---' map parallelism test: FAIL
  failed_any=1
fi

if cat mr-out* | grep '^parallel.* 2' > /dev/null
then
  echo '---' map parallelism test: PASS
else
  echo '---' map workers did not run in parallel
  echo '---' map parallelism test: FAIL
  failed_any=1
fi

wait

exit $failed_any