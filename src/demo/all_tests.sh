#!/usr/bin/env bash

# all tests

failed_any=0

./test_wc.sh || failed_any=1
./test_indexer.sh || failed_any=1
./test_early_exit.sh || failed_any=1
./test_mapparallel.sh || failed_any=1
./test_reduceparallel.sh || failed_any=1
./test_jobcount.sh || failed_any=1
./test_crash.sh || failed_any=1

#########################################################
if [ $failed_any -eq 0 ]; then
    echo '***' PASSED ALL TESTS
else
    echo '***' FAILED SOME TESTS
    exit 1
fi
