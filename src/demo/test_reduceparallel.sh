#!/usr/bin/env bash

# timeout command
TIMEOUT="timeout -k 2s 180s"
RACE=-race
failed_any=0

# run the test in a fresh sub-directory.
rm -rf mr-tmp
mkdir mr-tmp || exit 1
cd mr-tmp || exit 1
rm -f mr-*
cd ..

# make sure software is freshly built.
(cd .. && make mr) || exit 1
(cd ../applications && go build $RACE -buildmode=plugin rtiming.go) || exit 1

cd mr-tmp

echo '***' Starting reduce parallelism test.

rm -f mr-*

$TIMEOUT ../../main/mrcoordinator ../../text_files/*txt &
sleep 1

$TIMEOUT ../../main/mrworker ../../applications/rtiming.so &
$TIMEOUT ../../main/mrworker ../../applications/rtiming.so

NT=`cat mr-out* | grep '^[a-z] 2' | wc -l | sed 's/ //g'`
if [ "$NT" -lt "2" ]
then
  echo '---' too few parallel reduces.
  echo '---' reduce parallelism test: FAIL
  failed_any=1
else
  echo '---' reduce parallelism test: PASS
fi

wait

exit $failed_any