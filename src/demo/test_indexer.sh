#!/usr/bin/env bash

# timeout command
TIMEOUT="timeout -k 2s 180s"
RACE=-race
failed_any=0

# run the test in a fresh sub-directory.
rm -rf mr-tmp
mkdir mr-tmp || exit 1
cd mr-tmp || exit 1
rm -f mr-*
cd ..

# make sure software is freshly built.
(cd .. && make mr) || exit 1
(cd ../applications && go build $RACE -buildmode=plugin indexer.go) || exit 1

cd mr-tmp
rm -f mr-*

# generate the correct output
../../main/mrsequential ../../applications/indexer.so ../../text_files/*txt || exit 1
sort mr-out-0 > mr-correct-indexer.txt
rm -f mr-out*

echo '***' Starting indexer test.

$TIMEOUT ../../main/mrcoordinator ../../text_files/*txt &
sleep 1

# start multiple workers
$TIMEOUT ../../main/mrworker ../../applications/indexer.so &
$TIMEOUT ../../main/mrworker ../../applications/indexer.so

sort mr-out* | grep . > mr-indexer-all
if cmp mr-indexer-all mr-correct-indexer.txt
then
  echo '---' indexer test: PASS
else
  echo '---' indexer output is not the same as mr-correct-indexer.txt
  echo '---' indexer test: FAIL
  failed_any=1
fi

wait

exit $failed_any