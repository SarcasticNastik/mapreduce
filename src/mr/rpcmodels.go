package mr

/*
This file defines the RPCs that the master and workers use to
communicate with each other.
*/

import (
	"os"
	"strconv"
)

// RPC struct for worker to register itself to the coordinator
// so that it can be allotted a task to work on.
type GetTaskArgs struct {
	WorkerID []byte
}

/*
RPC struct for coordinator to reply to the worker with the task
to be performed. If reduce Task, SerialNo gives the output Task number
and FileName is ignored. If map Task, FileName is used and SerialNo is ignored.
*/
type GetTaskReply struct {
	SerialNo    int
	Task        WorkerType
	FileName    string
	NReduceTask int
	NMapTask    int
}

// RPC struct for worker to inform the coordinator that it has completed
// the task allotted to it.
type PutTaskArgs struct {
	WorkerID []byte
	Task     WorkerType
	Sno      int
}

// RPC struct for coordinator to acknowledge the worker that it has received
// the task completion notification.
type PutTaskReply struct {
	ACK bool
}

// Cook up a unique-ish UNIX-domain socket name
// in /var/tmp, for the coordinator.
// Can't use the current directory since
// Athena AFS doesn't support UNIX-domain sockets.
func coordinatorSock() string {
	s := "/var/tmp/mapreduce-mr-"
	s += strconv.Itoa(os.Getuid())
	return s
}
