package mr

/*
This file defines useful data types and constants used throughout the mr package.
*/


type WorkerType string
const (
	WAITING WorkerType = "pending task completion"
	ALLDONE WorkerType = "mapreduce tasks completed"
	MAP     WorkerType = "map"
	REDUCE  WorkerType = "reduce"
)


// ByKey for sorting by key.
type ByKey []string

// Len and other useful functions for sorting by key.
func (a ByKey) Len() int           { return len(a) }
func (a ByKey) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByKey) Less(i, j int) bool { return a[i] < a[j] }

