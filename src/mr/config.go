package mr

/*
This file defines the configuration parameters for the MapReduce framework.
This might need changing depending on the task at hand.
*/


import (
	"time"
)

// Time quanta used for various wait times throught the package.
const TIMEQUANTA = 300 * time.Millisecond

/* 
The amount of time the coordinator waits for a worker to complete a map task.
If the worker does not complete the task within this time, the coordinator
will mark the task as failed.
IN TIMEQUANTA UNITS
*/
const MAP_WORKER_ALLOTTED_TIME = 25

/*
The amount of time the coordinator waits for a worker to complete a reduce task.
If the worker does not complete the task within this time, the coordinator
will mark the task as failed.
IN TIMEQUANTA UNITS
*/
const REDUCE_WORKER_ALLOTTED_TIME = 25


/*
The amount of time the worker waits for a task to be allotted to it.
If the worker does not receive a task within this time, it will exit.
*/
const WORKER_TASK_WAIT_TIME = 100


/*
The amount of time the worker waits for an acknowledgement for a completed task
from the coordinator. If the worker does not receive a task within this time, 
it will exit.
*/
const WORKER_ACK_WAIT_TIME = 100