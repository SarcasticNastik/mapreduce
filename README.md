# Map Reduce Framework

## Directory Structure

* mr - the package containing the main map reduce code for coordinator and worker.
* main - contains the code to create objects
* applications - some map reduce applications to test the code with
* demo - scripts to test and demo the workings of the implementation.
* text_files - some input files for testing
* Makefile - contains compilation commands

Within each folder each file contains brief a description of what it contains.

<br>
<hr>
<br>


## Usage Instructions

For usign the map reduce framework your application needs to define 2 functions with signature as follows:
* func Map(filename string, contents string) []mr.KeyValue -  the mapper that takes as input the input filenaem and all its contents and outputs intermediate key value pairs.
* func Reduce(key string, values []string) string - the reducer that takes a key and its corresponding values as input and returns the final value as output.

Now compile your application with the command:
```go build -buildmode=plugin {path to application.go file}```

The input must already be presplit. The number of splits determine the number of mappers.

Now checkout and change if required the configuration from mr/config.go. You can also modify the number of reducers in main/mrcoordinator.go -  the second argument of MakeCoordinator.

To compile the map reduce framework run ```make mr``` from the src directory. This creates 3 executables coordinator, worker and mrsequential (for running sequentially for testing)

Run the coordinator using ```{mrcoordinator executable path} {input files dir} & ```.

Run multiple workers each using ```{mrworker executable path} {compiled .so file of application}```.

The output of the ith reducer will be stored in mr-out-i in the directory from which the commands were run.

<br>
<hr>
<br>

## Testing Instructions
To run individual test:
```
cd demo
bash {test .sh path} 2> log
```

To run all the tests once:
```
cd demo
bash all_tests.sh 2> log
```

To run tests multiple times (say 10) to check for race conditions:
```
cd demo
bash test-mr-many.sh 20 2> log
```